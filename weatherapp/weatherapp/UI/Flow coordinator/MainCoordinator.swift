//
//  MainCoordinator.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator : Coordinator, HomePageViewModelCoordinator, ForecastPageViewModelCoordinator {
    private let rootController: UITabBarController
    private let window: UIWindow
    
    private let apiService: WeatherAPIService
    private let locationService: LocationService
    private let remoteCacheService: RemoteCacheService
    
    init(with rootWindow: UIWindow) {
        self.window = rootWindow
        
        self.locationService = LocationService.init()
        self.apiService = WeatherAPIService.init()
        self.remoteCacheService = RemoteCacheService.init()
        
        let homeViewController = UIStoryboard.main.instantiateViewController(withIdentifier: "HomePageViewController") as! HomePageViewController
        let forecastViewController = UIStoryboard.main.instantiateViewController(withIdentifier: "ForecastPageViewController") as! ForecastPageViewController
        
        let homeContainerController = UINavigationController.init(rootViewController: homeViewController)
        let forecastContainerController = UINavigationController.init(rootViewController: forecastViewController)
        
        homeContainerController.tabBarItem = UITabBarItem(title: "Today", image: UIImage(named:"icon_tab_today"), tag: 0)
        forecastContainerController.tabBarItem = UITabBarItem(title: "Forecast", image: UIImage(named:"icon_tab_forecast"), tag: 1)
    
        self.rootController = UITabBarController.init()
        self.rootController.viewControllers =
            [homeContainerController,
             forecastContainerController]
        
        super.init()
        
        homeViewController.viewModel = HomePageViewModel(coordinator: self, apiService: apiService, locationService: locationService, remoteCacheService: remoteCacheService)
        forecastViewController.viewModel = ForecastPageViewModel(coordinator: self, apiService: apiService, locationService: locationService)
    }
    
    // MARK: - Coordinator overriden methods
    
    override func start() {
        self.window.rootViewController = self.rootController
        self.window.makeKeyAndVisible()
    }
    
    override func childCoordinatorDidFinishTask(coordinator: Coordinator) {
        //no child coordinators for now
    }
    
    
}
