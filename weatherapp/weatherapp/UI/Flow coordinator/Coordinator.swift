//
//  Coordinator.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import UIKit

class Coordinator {
    weak var rootCoordinator : Coordinator?
    func start(){}
    func childCoordinatorDidFinishTask(coordinator: Coordinator){}
    func childViewControllerDidFinishTask(viewController: UIViewController){}
}
