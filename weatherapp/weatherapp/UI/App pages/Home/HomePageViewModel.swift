//
//  HomePageViewModel.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import UIKit
import CoreLocation

protocol HomePageViewModelCoordinator {
    //In real life this protocol list all events that can be sent from the view controller (via view model) to it's coordinator. In this app there are no event to interchange, so this protocol is empty
}

protocol HomePageViewModelDisplayDelegate : class {
    func needsRefreshData()
}

class HomePageViewModel : LocationChangeObserver {
    private var coordinator: HomePageViewModelCoordinator
    private var locationService: LocationService
    private var apiService: WeatherAPIService
    private var remoteCacheService: RemoteCacheService
    
    private var currentWeather: WeatherEntry?
    
    weak var displayDelegate: HomePageViewModelDisplayDelegate?
    weak var alertDelegate: AlertDelegate?

    init (coordinator: HomePageViewModelCoordinator, apiService: WeatherAPIService, locationService: LocationService, remoteCacheService: RemoteCacheService) {
        self.coordinator = coordinator
        self.locationService = locationService
        self.apiService = apiService
        self.remoteCacheService = remoteCacheService
        
        locationService.addObserver(observer: self)
    }
    
    func checkLocationDataAvailability() {
        if !self.locationService.canEverProvideLocation {
            self.alertDelegate?.needsShowInfoAlert(title: "Error", body: "Location access either forbidden or restricted")
        }
    }
    
    //MARK: data providing accessors
    var temperature : String {
        get {
            if let temperature = currentWeather?.temperature {
                return String(format: "%0.0f°", temperature - 273)
            } else {
                return "--"
            }
        }
    }
    
    var cityName: String {
        get {
            return currentWeather?.cityName ?? "Loading..."
        }
    }
    
    var weatherDescription: String {
        get {
            return currentWeather?.skyDescription ?? "--"
        }
    }
    
    var weatherIconURL: URL? {
        get {
            return apiService.fullIconUrlForName(currentWeather?.iconName)
        }
    }
    
    var humidity: String {
        get {
            if let humidity = currentWeather?.humidity {
                return String(format: "%0.0f%%", humidity)
            } else {
                return "--"
            }
        }
    }
    
    var precipitations: String {
        get {
            if let precipitations = currentWeather?.rain {
                return String(format: "%0.1f mm%", precipitations)
            } else {
                return "--"
            }
        }
    }
    
    var pressure: String {
        get {
            if let pressure = currentWeather?.pressure {
                return String(format: "%0.0f hPa", pressure)
            } else {
                return "--"
            }
        }
    }
    
    var windSpeed: String {
        get {
            if let windSpeed = currentWeather?.windSpeed {
                return String(format: "%0.0f km/h", windSpeed)
            } else {
                return "--"
            }
        }
    }
    
    var windDirection: String {
        get {
            return currentWeather?.windDirectionString ?? "--"
        }
    }
    
    var shareString: String? {
        if let city = currentWeather?.cityName, let weather = currentWeather?.skyDescription, let temp = currentWeather?.temperature {
            return "The weather in \(city) is \(weather), temperature is \(temp - 273) degrees."
        } else {
            return nil
        }
    }
    
    

    //MARK: location change callbacks
    func locationServiceDidUpdateLocation(_ location: CLLocation, service: LocationService) {
        apiService.loadCurrentWeatherForCoordinates(lat: location.coordinate.latitude, lon: location.coordinate.longitude) { (weatherEntry, errors) in
            if let weatherEntry = weatherEntry {
                self.currentWeather = weatherEntry
                
                //in real life I'd probably add one more abstraction layer to rule data flows between remote cache, local cache and original data sources. But for now let's handle it right here.
                self.remoteCacheService.cacheUserData(weatherEntry)
                self.displayDelegate?.needsRefreshData()
            } else {
                self.remoteCacheService.requestCachedData { (entry, error) in
                    if entry != nil {
                        self.currentWeather = entry
                        self.displayDelegate?.needsRefreshData()
                    } else {
                        self.alertDelegate?.needsShowInfoAlert(title: "Error", body: "Error communicating with the server and with the remote cache")
                    }
                }
            }
        }
    }
    
    func locationServiceDidFailWithError(_ error: Error, service: LocationService) {
        self.remoteCacheService.requestCachedData { (entry, error) in
            if entry != nil {
                self.currentWeather = entry
                self.displayDelegate?.needsRefreshData()
            } else {
                self.alertDelegate?.needsShowInfoAlert(title: "Error", body: "Unable to obtain current location and can't access remote cache")
            }
        }
    }
}
