//
//  HomePageViewController.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import UIKit
import Kingfisher

class HomePageViewController: UIViewController, HomePageViewModelDisplayDelegate, AlertDelegate {
    
    var viewModel: HomePageViewModel! //In general force unwrapping is bad, but if the view model is nil, something is terribly wrong. As this can't be avoided gracefully, force unwraping is the least problem we have.
    
    @IBOutlet private var weatherImageview : UIImageView?
    @IBOutlet private var temperatureLabel : UILabel?
    @IBOutlet private var conditionsLabel : UILabel?
    @IBOutlet private var locationLabel : UILabel?
    @IBOutlet private var extendedInfoLabels : [UILabel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named:"navigation_bg"), for: .default)
        
        redrawContents()

        viewModel.displayDelegate = self
        viewModel.alertDelegate = self
        
        viewModel.checkLocationDataAvailability()
    }
    
    func redrawContents() {
        weatherImageview?.kf.setImage(with: viewModel.weatherIconURL)
        
        temperatureLabel?.text = viewModel.temperature
        locationLabel?.text = viewModel.cityName
        conditionsLabel?.text = viewModel.weatherDescription
        
        extendedInfoLabels[0].text = viewModel.humidity
        extendedInfoLabels[1].text = viewModel.windSpeed
        extendedInfoLabels[2].text = viewModel.precipitations
        extendedInfoLabels[3].text = viewModel.windDirection
        extendedInfoLabels[4].text = viewModel.pressure
        
    }
    
    @IBAction func share() {
        if let shareString = viewModel.shareString {
            let avc = UIActivityViewController.init(activityItems: [shareString], applicationActivities: nil)
            self.present(avc, animated: true, completion: nil)
        }
    }
    
    //MARK: display delegate methods
    func needsRefreshData() {
        redrawContents()
    }

    
    //MARK: alert delegate methods
    func needsShowInfoAlert(title: String, body: String) {
        let a = UIAlertController(title: title, message: body, preferredStyle: .alert)
        a.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(a, animated: true, completion: nil)
    }

}
