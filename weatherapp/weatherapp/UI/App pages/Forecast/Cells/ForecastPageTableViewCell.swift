//
//  ForecastPageTableViewCell.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import UIKit
import Kingfisher

class ForecastPageTableViewCell: UITableViewCell {

    @IBOutlet private var conditionsImage: UIImageView?
    @IBOutlet private var timeLabel: UILabel?
    @IBOutlet private var conditionsLabel: UILabel?
    @IBOutlet private var degreesLabel: UILabel?
    
    var viewModel: ForecastPageCellViewModel! {
        didSet {
            updateContent()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateContent() {
        timeLabel?.text = viewModel.time
        conditionsLabel?.text = viewModel.weatherDescription
        degreesLabel?.text = viewModel.temperature
        
        if let imageUrl = viewModel.weatherIconURL {
            KingfisherManager.shared.retrieveImage(with: imageUrl) { result in
                switch result {
                case .success(let imageResult):
                    if (imageResult.source.url == self.viewModel.weatherIconURL) {
                        //kingfisher still stays within the main thread, so updating UI is ok
                        self.conditionsImage?.image = imageResult.image
                    }
                case .failure(_): break //nothing we can really do here
                }
            }
        }
    }

}
