//
//  ForecastPageCellViewModel.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import UIKit

class ForecastPageCellViewModel: NSObject {
    
    private let weatherEntry: WeatherEntry
    private let iconURL: URL?
    private let dateFormatter: DateFormatter
    
    init(weather: WeatherEntry, iconURL: URL?) {
        self.weatherEntry = weather
        self.iconURL = iconURL
        self.dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "HH:mm"
    }
    
    //MARK: data providing accessors
    var temperature : String {
        get {
            return String(format: "%0.0f°", weatherEntry.temperature - 273)
        }
    }
    
    var weatherDescription: String {
        get {
            return weatherEntry.skyDescription ?? "--"
        }
    }
    
    var weatherIconURL: URL? {
        get {
            return iconURL
        }
    }
    
    var time: String {
        get {
            return dateFormatter.string(from: weatherEntry.date)
        }
    }
    
    
}
