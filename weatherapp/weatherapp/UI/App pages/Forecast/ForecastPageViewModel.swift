//
//  ForecastPageViewModel.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import UIKit
import CoreLocation

protocol ForecastPageViewModelCoordinator {
    //In real life this protocol list all events that can be sent from the view controller (via view model) to it's coordinator. In this app there are no event to interchange, so this protocol is empty
}

protocol ForecastPageViewModelDisplayDelegate : class {
    func needsRefreshData()
}


class ForecastPageViewModel : LocationChangeObserver {
    
    
    var coordinator: ForecastPageViewModelCoordinator
    var locationService: LocationService
    var apiService: WeatherAPIService
    
    var entriesByDays: [[WeatherEntry]] = [[]]

    weak var displayDelegate: ForecastPageViewModelDisplayDelegate?
    weak var alertDelegate: AlertDelegate?

    init (coordinator: ForecastPageViewModelCoordinator, apiService: WeatherAPIService, locationService: LocationService) {
        self.coordinator = coordinator
        self.apiService = apiService
        self.locationService = locationService
        
        self.locationService.addObserver(observer: self)
    }
    
    var listTitle: String {
        get {
            if let entry = entriesByDays.first?.first {
                return entry.cityName ?? "--"
            } else {
                return "--"
            }
        }
    }
    
    func sectionsCount() -> Int {
        return entriesByDays.count
    }
    
    func entriesCountInSection(_ section: Int) -> Int {
        return entriesByDays[section].count
    }
    
    func sectionTitleForSection(_ section: Int) -> String {
        //This will fail in the last three hours of the current day.
        if let date = entriesByDays[section].first?.date {
            if Calendar.current.isDate(date, inSameDayAs: Date.init()) {
                return "TODAY"
            } else {
                let formatter = DateFormatter.init()
                formatter.dateFormat = "eeee"
                return formatter.string(from: date).uppercased()
            }
        } else {
            return "----"
        }
        
    }
    
    func cellViewModelForIndexPath(_ indexPath: IndexPath) -> ForecastPageCellViewModel {
        let entry = entriesByDays[indexPath.section][indexPath.row]
        return ForecastPageCellViewModel.init(weather: entry, iconURL: apiService.fullIconUrlForName(entry.iconName))
    }
    
    //MARK: location service handlers
    func locationServiceDidUpdateLocation(_ location: CLLocation, service: LocationService) {
        self.apiService.loadWeatherForecastForCoordinates(lat: location.coordinate.latitude, lon: location.coordinate.longitude) { (allEntries, errors) in
            
            if let allEntries = allEntries, errors.count == 0 {
                self.entriesByDays = []
                let calendar = NSCalendar.current
                
                var currentDay = calendar.component(.day, from: allEntries.first?.date ?? Date.init())
                var currentSectionContents: [WeatherEntry] = []
                
                //assumption: all days are strictly in sequence, no missing dates
                for entry in allEntries {
                    let day = calendar.component(.day, from: entry.date)
                    if day == currentDay {
                        currentSectionContents.append(entry)
                    } else {
                        self.entriesByDays.append(currentSectionContents)
                        currentSectionContents = [entry]
                        currentDay = day
                    }
                }
                self.displayDelegate?.needsRefreshData()
            } else {
                self.alertDelegate?.needsShowInfoAlert(title: "Error", body: "Error communicating with the server")
            }
        }
    }
    
    func locationServiceDidFailWithError(_ error: Error, service: LocationService) {
        //do not indicate in any way
    }
}
