//
//  ForecastPageViewController.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import UIKit

class ForecastPageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ForecastPageViewModelDisplayDelegate, AlertDelegate {

    @IBOutlet private var tableView: UITableView?
    
    var viewModel: ForecastPageViewModel! //In general force unwrapping is bad, but if the view model is nil, something is terribly wrong. As this can't be avoided gracefully, force unwraping is the least problem we have.

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.displayDelegate = self
        viewModel.alertDelegate = self

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named:"navigation_bg"), for: .default)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = viewModel.listTitle
    }
    
    //MARK: table view data source and delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionsCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.entriesCountInSection(section)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.sectionTitleForSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //String literals usually should be kept separately as constants, but I don't excract constants that are used in a single place and are not a subject to change frequently
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastPageTableViewCell", for: indexPath) as! ForecastPageTableViewCell
        cell.viewModel = self.viewModel.cellViewModelForIndexPath(indexPath)
        return cell
    }
    
    //MARK: display delegate methods
    func needsRefreshData() {
        tableView?.reloadData()
        self.navigationItem.title = viewModel.listTitle
    }

    //MARK: alert delegate methods
    func needsShowInfoAlert(title: String, body: String) {
        let a = UIAlertController(title: title, message: body, preferredStyle: .alert)
        a.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(a, animated: true, completion: nil)
    }
}
