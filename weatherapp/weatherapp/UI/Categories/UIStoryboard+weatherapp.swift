//
//  UIStoryboard+weatherapp.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static var main : UIStoryboard {
        get {
            return UIStoryboard.init(name: "Main", bundle: nil)
        }
    }
}
