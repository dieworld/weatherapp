//
//  WeatherEntry+WindDirection.swift
//  weatherapp
//
//  Created by IC on 05/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import Foundation

extension WeatherEntry {
    var windDirectionString : String {
        get {
            //assume API always returns valid direction between 0 and 360 degrees
            guard let windDirection = windDirection else {
                return "--"
            }
            
            let directions = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
            let sectorSize = 360.0 / Double(directions.count)
            var centralizedDirection = windDirection + 360.0 / 16

            while centralizedDirection >= 360 {
                centralizedDirection -= 36
            }
            
            let sectorIndex = Int(centralizedDirection / sectorSize)
            return directions[sectorIndex]
        }
    }
}
