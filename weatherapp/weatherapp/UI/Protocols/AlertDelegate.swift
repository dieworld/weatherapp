//
//  AlertDelegate.swift
//  weatherapp
//
//  Created by IC on 05/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import Foundation

protocol AlertDelegate : class {
    func needsShowInfoAlert(title: String, body: String)
}
