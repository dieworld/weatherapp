//
//  LocationService.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import Foundation
import CoreLocation

enum LocationError : Error {
    case LocationServicesUnavailable
}

protocol LocationChangeObserver : class {
    func locationServiceDidUpdateLocation(_ location: CLLocation, service: LocationService)
    func locationServiceDidFailWithError(_ error: Error, service: LocationService)
}

class LocationService : NSObject, CLLocationManagerDelegate {
    
    private var observers: NSPointerArray = NSPointerArray.weakObjects()
    private let locationManager: CLLocationManager
    public var lastLocation: CLLocation? {
        get {
            return locationManager.location
        }
    }
    
    public var canEverProvideLocation: Bool {
        get {
            let status = CLLocationManager.authorizationStatus()
            return status != .denied && status != .restricted
        }
    }
    
    
    override init() {
        locationManager = CLLocationManager.init()
        
        super.init()

        locationManager.delegate = self
        locationManager.distanceFilter = 100
    
        if (CLLocationManager.authorizationStatus() == .authorizedWhenInUse) {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func addObserver(observer: LocationChangeObserver) {
        self.observers.addPointer(Unmanaged.passUnretained(observer as AnyObject).toOpaque())
    }
    
    private func notifyObserversWithError(_ error: Error) {
        observers.compact()
        for index in 0..<self.observers.count {
            guard let observerPointer = observers.pointer(at: index) else {
                continue
                
            }
            
            guard let observer = Unmanaged<AnyObject>.fromOpaque(observerPointer).takeUnretainedValue() as? LocationChangeObserver  else {
                continue
            }
            
            observer.locationServiceDidFailWithError(error, service: self)
        }
    }
        
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        notifyObserversWithError(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let location = manager.location else {
            return
        }
        
        observers.compact()
        for index in 0..<self.observers.count {
            guard let observerPointer = observers.pointer(at: index) else {
                continue
                
            }
            
            guard let observer = Unmanaged<AnyObject>.fromOpaque(observerPointer).takeUnretainedValue() as? LocationChangeObserver  else {
                continue
            }
            
            observer.locationServiceDidUpdateLocation(location, service: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedWhenInUse) {
            manager.startUpdatingLocation()
        } else if (status == .denied || status == .restricted) {
            notifyObserversWithError(LocationError.LocationServicesUnavailable)
        }
    }
}
