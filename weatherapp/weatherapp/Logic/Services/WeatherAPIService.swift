//
//  WeatherAPIService.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

fileprivate let IconsRoot = "https://openweathermap.org/img/w/"
fileprivate let APIRoot = "https://api.openweathermap.org/data/2.5/"
fileprivate let APIKey : String = "54c785d8833e52c30cce51ad1a01c540"

enum APIErrors : Error {
    case unknownError
    case malformedResponse
}

enum APIPaths : String {
    case today = "weather"
    case forecast = "forecast"
}

class WeatherAPIService {
    
    init() {
        
    }
    
    func loadCurrentWeatherForCoordinates(lat: Double, lon: Double, completion: @escaping (_ : WeatherEntry?, _ errors: [Error]) -> ()) {
        
        let requestLink = APIRoot + APIPaths.today.rawValue
        let requestURL = URL.init(string: requestLink)! //! is ok because relies on constants
        let parameters: [String: Any] = ["lat" : lat, "lon" : lon, "appid" : APIKey]
        
       SessionManager.default
        .request(requestURL, method: .get, parameters: parameters)
        .validate()
        .responseJSON { (response) in
            guard response.result.isSuccess, let data = response.data else {
                if let error = response.result.error {
                    completion(nil, [error])
                } else {
                    completion(nil, [APIErrors.unknownError])
                }
                return
            }
            
            guard let json = try? JSON.init(data: data) else {
                completion(nil, [APIErrors.malformedResponse])
                return
            }
            
            //again, since those keys are used only in this single place across the whole app, I keep em as literals.
            //in case of more consistent mapping between requests, I could use ObjectMapper and construct objects right from JSON data in weatherentry's constructor
            
            //We're assuming that if the server responds, it's response is correct
            //we consider something is wrong if we miss temperature. missing any other value is ok for us
            guard let temperature = json["main"]["temp"].double else {
                completion(nil, [APIErrors.malformedResponse])
                return
            }


            var cityName = json["name"].string
            if cityName != nil {
                if let country = json["sys"]["country"].string {
                    cityName? += ", " + country
                }
            }
            
            let skyDesc = json["weather"].array?.first?["description"].string
            let skyIcon = json["weather"].array?.first?["icon"].string
            let humidity = json["main"]["humidity"].double
            let pressure = json["main"]["pressure"].double
            let wind = json["wind"]["speed"].double
            let windDir = json["wind"]["deg"].double
            let rainfall = json["rain"]["3h"].double
            
            let weatherEntry =
                WeatherEntry.init(date: Date.init(),
                                  cityName: cityName,
                                  temperature: temperature,
                                  humidity: humidity,
                                  pressure: pressure,
                                  windSpeed: wind,
                                  windDirection: windDir,
                                  rain: rainfall,
                                  skyDescription: skyDesc,
                                  iconName: skyIcon)
            completion(weatherEntry, [])
        }
    }
    
    
    func loadWeatherForecastForCoordinates(lat: Double, lon: Double, completion: @escaping (_ : [WeatherEntry]?, _ errors: [Error]) -> ()) {
        
        let requestLink = APIRoot + APIPaths.forecast.rawValue
        let requestURL = URL.init(string: requestLink)! //! is ok because relies on constants
        let parameters: [String: Any] = ["lat" : lat, "lon" : lon, "appid" : APIKey]
        
        SessionManager.default
            .request(requestURL, method: .get, parameters: parameters)
            .validate()
            .responseJSON { (response) in
                guard response.result.isSuccess, let data = response.data else {
                    if let error = response.result.error {
                        completion(nil, [error])
                    } else {
                        completion(nil, [APIErrors.unknownError])
                    }
                    return
                }
                
                guard let containerJson = try? JSON.init(data: data) else {
                    completion(nil, [APIErrors.malformedResponse])
                    return
                }
                
                let cityName = containerJson["city"]["name"].stringValue

                
                var decodedEntries: [WeatherEntry] = []
                let entriesJson = containerJson["list"].arrayValue
                for json in entriesJson {
                    guard let temperature = json["main"]["temp"].double else {
                        continue
                    }
                    
                    let skyDesc = json["weather"].array?.first?["description"].string
                    let skyIcon = json["weather"].array?.first?["icon"].string
                    let humidity = json["main"]["humidity"].double
                    let pressure = json["main"]["pressure"].double
                    let windSpeed = json["wind"]["speed"].double
                    let windDir = json["wind"]["deg"].double
                    let rainfall = json["rain"]["3h"].double
                    let date = Date.init(timeIntervalSince1970: json["dt"].doubleValue)

                    

                    let weatherEntry =
                        WeatherEntry.init(date: date,
                                          cityName: cityName,
                                          temperature: temperature,
                                          humidity: humidity,
                                          pressure: pressure,
                                          windSpeed: windSpeed,
                                          windDirection: windDir,
                                          rain: rainfall,
                                          skyDescription: skyDesc,
                                          iconName: skyIcon)
                    
                    decodedEntries.append(weatherEntry)
                }
                
                decodedEntries.sort(by: { $0.date.compare($1.date) == .orderedAscending })
                
                completion(decodedEntries, [])
        }
    }
    
    func fullIconUrlForName(_ name: String?) -> URL? {
        guard let name = name else {
            return nil
        }
        
        let link = IconsRoot + name + ".png"
        let url = URL.init(string: link)! //based on constants
        return url
    }
}
