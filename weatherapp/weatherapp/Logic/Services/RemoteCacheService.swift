//
//  RemoteCacheService.swift
//  weatherapp
//
//  Created by IC on 05/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import Foundation
import FirebaseDatabase

private let UserTokenKey = "com.ixobit.weatherapp.usertoken"

private let DatabaseName = "userCaches"

enum RemoteCacheErrors: Error {
    case MalformedStoredData
}

class RemoteCacheService {
    var ref: DatabaseReference
    var userToken: String
        
    init () {
        ref = Database.database().reference()
        
        if let userToken = UserDefaults.standard.string(forKey: UserTokenKey) {
            self.userToken = userToken
        } else {
            self.userToken = UUID.init().uuidString
            UserDefaults.standard.set(self.userToken, forKey: UserTokenKey)
        }
    }
    
    func cacheUserData(_ entry: WeatherEntry) {
        let dataDict: [String: Any] =
            ["iconName" : entry.iconName ?? "",
             "locationName": entry.cityName ?? "",
             "skydescription": entry.skyDescription ?? "",
             "temperature": entry.temperature]
        
        ref.child(DatabaseName).child(self.userToken).setValue(dataDict)
    }
    
    func requestCachedData(completion: @escaping (_ entry: WeatherEntry?, _ error: Error?) -> ()) {
       
        ref.child(DatabaseName).child(self.userToken).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            if let value = snapshot.value as? NSDictionary {
                guard let temperature = value.object(forKey: "temperature") as? Double else {
                    completion(nil, RemoteCacheErrors.MalformedStoredData)
                    return
                }
                
                let entry = WeatherEntry(date: Date.init(),
                                         cityName: value.object(forKey: "locationName") as? String,
                                         temperature: temperature,
                                         humidity: nil,
                                         pressure: nil,
                                         windSpeed: nil,
                                         windDirection: nil,
                                         rain: nil,
                                         skyDescription: value.object(forKey: "skydescription") as? String,
                                         iconName: value.object(forKey: "iconName") as? String)
                
                completion(entry, nil)
            } else {
                completion(nil, RemoteCacheErrors.MalformedStoredData)
            }
            
        }) { (error) in
            completion(nil, error)
        }
    }
}
