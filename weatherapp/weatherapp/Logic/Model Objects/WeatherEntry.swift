//
//  WeatherEntry.swift
//  weatherapp
//
//  Created by IC on 04/04/2019.
//  Copyright © 2019 ixobit. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WeatherEntry {
    let date: Date
    let cityName: String?
    let temperature: Double
    let humidity: Double?
    let pressure: Double?
    let windSpeed: Double?
    let windDirection: Double?
    let rain: Double?
    let skyDescription: String?
    let iconName: String?
}
